from Crypto.Cipher import AES
import base64

AES_BLOCK_SIZE = 16
AES_KEY_SIZE = 16


def xor(array1, array2):
    array1_xor_array2 = bytes(a ^ b for (a, b) in zip(array1, array2))
    return array1_xor_array2


def add_padding(data):  # pkcs7 padding
    pad_value = AES_BLOCK_SIZE - len(data) % AES_BLOCK_SIZE
    if pad_value == 0:
        return data + bytearray(chr(AES_BLOCK_SIZE) * AES_BLOCK_SIZE, "utf8")
    return data + bytearray(chr(pad_value) * pad_value, "utf8")


def remove_padding(data):
    pad_value = data[len(data) - 1]
    return data[0:len(data) - pad_value]


def read_from_file(filename):
    with open(filename, "rb") as f:
        data = f.read()
    return data


def write_to_file(filename, data):
    with open(filename, "wb") as f:
        f.write(data)


def aes_CBC_encrypt(data, key, iv):
    encrypted_data = b''
    cipher = AES.new(key, AES.MODE_ECB)
    for i in range(0, len(data), AES_BLOCK_SIZE):
        in_block = xor(data[i: i + AES_BLOCK_SIZE], iv)
        out_block = cipher.encrypt(in_block)
        iv = out_block
        encrypted_data = encrypted_data + out_block
    return encrypted_data


def aes_CBC_decrypt(encrypted_data, key, iv):
    data = b''
    cipher = AES.new(key, AES.MODE_ECB)
    for i in range(0, len(encrypted_data), AES_BLOCK_SIZE):
        in_block = encrypted_data[i: i + AES_BLOCK_SIZE]
        out_block = cipher.decrypt(in_block)
        out_block = xor(out_block, iv)
        iv = in_block
        data = data + out_block
    return data


def prog1():  # decriptare fisier criptopals
    key = bytearray("YELLOW SUBMARINE", "utf-8")
    iv = bytearray(16)

    encrypted_data = read_from_file("10.txt")

    encrypted_data = base64.b64decode(encrypted_data)

    data = aes_CBC_decrypt(encrypted_data, key, iv)
    data = remove_padding(data)
    write_to_file("after_encryption.txt", data)


def prog2():  # criptare fisier, decriptare, xor intre fisierul original si cel decriptat
    key = bytearray("YELLOW SUBMARINE", "utf-8")
    iv = bytearray(16)

    data = read_from_file("text.txt")

    data = add_padding(data)
    after_encryption = aes_CBC_encrypt(data, key, iv)
    write_to_file("after_encryption.txt", after_encryption)

    after_decryption = aes_CBC_decrypt(after_encryption, key, iv)
    after_decryption = remove_padding(after_decryption)
    write_to_file("after_decryption.txt", after_decryption)

    result = xor(data, after_decryption)

    for b in result:
        if b != 0:
            print("Fisierul decriptat NU coincide cu cel original")
            exit(0)

    print("Fisierul decriptat coincide cu cel original")


def main():
    prog2()


if __name__ == "__main__":
    main()
