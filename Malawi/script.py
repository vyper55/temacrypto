import codecs

from isqrt import isqrt


def modInverse(a, m):
    a = a % m
    for x in range(1, m):
        if (a * x) % m == 1:
            return x
    return 1


def get_params(file):
    with open(file, "r") as f:
        e = f.readline()
        n = f.readline()
        c = f.readline()
    e = int(e)
    n = int(n)
    c = int(c)
    return e, n, c


def factorize(n): #factorizare n folosind teorema lui fermat
    k = isqrt(n)
    k = k + 1

    while True:
        h = k ** 2 - n
        if isqrt(h) ** 2 == h:
            h = isqrt(h)
            break
        k = k + 1
    p = k + h
    q = k - h
    return p, q


def decrypt(p, q, e, c):
    d = pow(e, -1, (p - 1) * (q - 1))
    m = pow(c, d, p * q)
    m = m % pow(2, 200)
    m = hex(m)
    m = str(m)[2:len(m)]
    msg = bytes.fromhex(m).decode('utf-8')
    return msg


def main():
    e, n, c = get_params("parameters")
    # p, q = factorize(n)  # dureaza cam 10 min
    p = 17777324810733646969488445787976391269105128850805128551409042425916175469483806303918279424710789334026260880628723893508382860291986009694703181381742497
    q = 17777324810733646969488445787976391269105128850805128551409042425916175469168770593916088768472336728042727873643069063316671869732507795155086000807594027

    print("Flag is: " + decrypt(p, q, e, c))


if __name__ == "__main__":
    main()
